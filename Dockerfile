FROM openjdk:15-jdk-alpine

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Path to the app JAR file
ARG JAR_FILE=target/recruitment-0.0.1-SNAPSHOT.jar

# Add the above JAR to the container
ADD ${JAR_FILE} app.jar

# Run the JAR on the start of the container
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=cloud","-jar","/app.jar"]


