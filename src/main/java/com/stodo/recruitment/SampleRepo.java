package com.stodo.recruitment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepo extends CrudRepository<SampleEntity, Long> {
}
