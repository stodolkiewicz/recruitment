package com.stodo.recruitment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleRestController {

    @Autowired
    SampleRepo sampleRepo;

    @Value("${spring.datasource.password}")
    String password;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        sampleRepo.save(new SampleEntity(2L, "anotherOne!"));
        return "hello " + name + "The password is: " + password;
    }

    @PostMapping("/save")
    public void saveSampleEntity(String name) {
        sampleRepo.save(new SampleEntity("anotherOne!"));
    }

    @GetMapping
    public Iterable<SampleEntity> getAllSampleEntities() {
        return sampleRepo.findAll();
    }

}
